import axios from 'axios'
import Vue from 'vue'

const AxiosPlugin = {
    install(Vue) {
        Vue.prototype.$axios = axios.create({
            // TODO: get from dotenv file
            // baseURL: process.env.VUE_APP_API_BASE
            baseURL: 'http://localhost:8080/api/v1'
        })
    }
}

Vue.use(AxiosPlugin)

export default AxiosPlugin