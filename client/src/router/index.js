import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: require('@/views/HomeView').default
    },
    {
      path: '/orders',
      name: 'order',
      component: require('@/views/OrderView').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

export default router