module.exports = {
  devServer: {
    port: process.env.NODE_ENV === 'development' ? 3000: 4000
  },
  "transpileDependencies": [
    "vuetify"
  ]
}