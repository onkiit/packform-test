# Simple UI

## Prerequisites and Dependencies
1. Yarn
2. Vue JS
3. Vue Router
4. Vuetify

## Development Mode
1. Install all its dependencies by executing command `yarn install`
2. Run `yarn serve` to start
3. Go to `http://localhost:3000` through the browser

## Known issue
1. Currently for the `axios` base URL harcoded because still facing with issue with dotenv file that unable to parsed under plugins instance. So if the server is running with different port, the axios base URL need to be adjusted. https://gitlab.com/onkiit/packform-test/-/blob/master/client/src/plugins/axios.js#L9
