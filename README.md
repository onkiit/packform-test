# Simple web application 
Simple web application server which utilize some golang packages like 
- `github.com/gorilla/mux` as the http router
- `github.com/gorilla/handlers` as a reusable middleware for mux
- `github.com/spf13/cobra` as the command line helper
- `github.com/spf13/viper` as configuration helper
- `entgo.io/ent` as the ORM
- `github.com/lib/pq` as the driver of PostgreSQL
- `github.com/gocarina/gocsv` as the csv helper

## Prerequisites
1. Golang 1.18 or higher
2. PostgreSQL 14.x
3. Node JS
4. Yarn

## Additional
1. Docker Engine
2. docker-compose

## Run Web Server
1. Go to `server/cmd/app`
2. Copy configuration file by running command `cp app.yaml.example ./app.yaml`
3. Adjust configuration properties with your own environment
4. Fetch all dependencies by running command `go mod download`
5. Build project `go build`
6. Migrate the database table by running command `./app db-migrate`
7. Migrate the prepared data by running command `./app data-migrate`
8. Run server `./app`

## Run UI
NOTE: described under `client` folder 

## Running with docker (still not automated, need to do some stuff)
1. Go to `server/cmd/app`
2. Copy configuration file by running command `cp app.yaml.example ./app.yaml`
4. Go to root project
5. Copy environment file `cp .env.example.example ./.env`
6. Adjust your database configuration 
7. Adjust web server configuration (the *database* host should be `db_host` as configured on `extra_hosts`)
8. Run `docker-compose up` and wait until finished
9. Go to `webapi` container, run `docker-compose ps` to make sure the running status
10. Run `docker-compose exec webapi bash`
11. Run database migration by running command `./app db-migrate`
13. Run data migration by running command `./app data-migrate`
14. Exit from service
15. Try to access through browser by visiting `http://localhost:3000`