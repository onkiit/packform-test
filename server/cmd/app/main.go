package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/onkiit/packform-test/server/pkg/app"
	_ "gitlab.com/onkiit/packform-test/server/svc/order"
)

func main() {
	app.Run()
}
