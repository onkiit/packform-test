package config

type Configuration struct {
	Server     Server     `yaml:"server" mapstructure:"server"`
	Connection Connection `yaml:"connection" mapstructure:"connection"`
}

type Server struct {
	Host    string `yaml:"host" mapstructure:"host" json:"host,omitempty"`
	Port    string `yaml:"port" mapstructure:"port" json:"port,omitempty"`
	APIBase string `yaml:"api_base" mapstructure:"api_base" json:"api_base,omitempty"`
	CORS    CORS   `yaml:"cors" mapstructure:"cors"`
}

type CORS struct {
	AllowedOrigins []string `yaml:"allowed_origins" mapstructure:"allowed_origins"`
}

type Connection struct {
	SQL SQL `yaml:"sql" mapstructure:"sql"`
}

type SQL struct {
	Enabled bool   `yaml:"enabled" mapstructure:"enabled"`
	Driver  string `yaml:"driver" mapstructure:"driver"`
	URI     string `yaml:"uri" mapstructure:"uri"`
}
