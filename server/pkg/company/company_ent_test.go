package company

import (
	"context"
	"testing"

	"gitlab.com/onkiit/packform-test/server/ent/company"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
	"gitlab.com/onkiit/packform-test/server/pkg/helper"
)

type args struct {
	ctx   context.Context
	datas []Company
}

type testFunc func(test) func(*testing.T)

type test struct {
	name     string
	args     args
	wantErr  bool
	testFunc testFunc
}

func Test_companyEnt_BulkCreate(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	if err := helper.OpenDB(); err != nil {
		t.Error(err)
	}
	defer entgo.Close()
	ctx := context.Background()

	tests := []test{
		{
			name: "test bulk insert",
			args: args{
				ctx: ctx,
				datas: []Company{
					{
						ID:   1,
						Name: "Dummy Data 1",
					},
				},
			},
			wantErr: false,
			testFunc: func(tt test) func(*testing.T) {
				return func(t2 *testing.T) {
					store := NewStoreEnt()

					if err := store.BulkCreate(tt.args.ctx, tt.args.datas); err != nil {
						t.Error(err)
					}

					count, err := entgo.DB().Company.Query().Count(tt.args.ctx)
					if err != nil {
						t.Error(err)
					}

					if len(tt.args.datas) != count {
						t.Errorf("expected count %d but got %d", len(tt.args.datas), count)
					}

					// delete data the end
					for _, data := range tt.args.datas {
						if _, err := entgo.DB().Company.Delete().Where(company.IDEQ(data.ID)).Exec(tt.args.ctx); err != nil {
							t.Error("err", err)
						}
					}
				}
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, tt.testFunc(tt))
	}
}
