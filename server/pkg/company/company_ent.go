package company

import (
	"context"
	"log"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

type companyEnt struct{}

func (c *companyEnt) BulkCreate(ctx context.Context, datas []Company) error {

	return entgo.WithTx(ctx, entgo.DB(), func(tx *ent.Tx) error {
		for _, data := range datas {
			_, err := tx.Company.
				Create().
				SetID(data.ID).
				SetName(data.Name).
				Save(ctx)
			if err != nil {
				log.Println("create:", err)
				return tx.Rollback()
			}
		}
		return nil
	})
}

func NewStoreEnt() Store {
	return &companyEnt{}
}
