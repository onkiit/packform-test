package company

import "context"

type Company struct {
	ID   int    `json:"id" csv:"company_id"`
	Name string `json:"name" csv:"company_name"`
}

type Store interface {
	BulkCreate(context.Context, []Company) error
}
