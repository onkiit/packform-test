package api

import (
	"net/http"

	"gitlab.com/onkiit/packform-test/server/config"
)

var routers []RouterFactory

type APIRouter struct {
	Handler http.Handler
	Methods string
	Path    string
}

type RouterFactory func(*config.Configuration) Router

type Router interface {
	AppendRouter(string) []APIRouter
}

func Register(r RouterFactory) {
	routers = append(routers, r)
}

func Routers() []RouterFactory {
	return routers
}
