package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/onkiit/packform-test/server/config"
	"gitlab.com/onkiit/packform-test/server/pkg/app/api"
	"gitlab.com/onkiit/packform-test/server/pkg/command"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
	"gitlab.com/onkiit/packform-test/server/pkg/middleware"
)

func Run() {
	appname := filepath.Base(os.Args[0])
	v := viper.New()
	v.SetConfigType("yaml")
	v.SetConfigName(appname)
	v.SetDefault("port", ":8080")
	v.SetDefault("host", "localhost")
	v.AddConfigPath(".")

	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println(fmt.Errorf("load configuration: %w", err))
		}
	}
	var conf config.Configuration
	if err := v.Unmarshal(&conf); err != nil {
		log.Fatal("unmarshal config", err)
	}

	rootCmd := initializeCommands(&conf)
	// Put all registered commands to main command
	for _, f := range command.Commands() {
		cmd := f(&conf)
		rootCmd.AddCommand(cmd)
	}

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func initializeCommands(conf *config.Configuration) *cobra.Command {
	cmd := &cobra.Command{
		Use:   filepath.Base(os.Args[0]),
		Short: "packform-test",
		Long:  "Simple web application to complete Packform Test",
		Run: func(cmd *cobra.Command, args []string) {
			r := mux.NewRouter()
			r.Use(middleware.TraceMiddleware())
			for _, rt := range api.Routers() {
				rFactory := rt(conf)
				appendedRouters := rFactory.AppendRouter(conf.Server.APIBase)
				for _, router := range appendedRouters {
					// List all registered routes
					// Put all registered routers into main router
					log.Printf("Route: Method %s Path %s", router.Methods, router.Path)
					r.HandleFunc(router.Path, router.Handler.ServeHTTP).Methods(router.Methods)
				}
			}

			// Database open connection
			if conf.Connection.SQL.Enabled {
				if err := entgo.OpenConnection(entgo.Options{
					Driver: conf.Connection.SQL.Driver,
					URI:    conf.Connection.SQL.URI,
				}); err != nil {
					log.Fatal(err)
				}
			}

			handler := handlers.CORS(
				handlers.AllowedOrigins(conf.Server.CORS.AllowedOrigins),
				handlers.AllowedHeaders([]string{"Access-Control-Allow-Origin", "*"}),
				handlers.AllowedMethods([]string{http.MethodGet}),
			)(r)

			httpServer := http.Server{
				Addr:    fmt.Sprintf("%s%s", conf.Server.Host, conf.Server.Port),
				Handler: handler,
			}

			idleConnectionsClosed := make(chan struct{})
			log.Printf("Running http server %s%s. Base API %s", conf.Server.Host, conf.Server.Port, conf.Server.APIBase)

			sigint := make(chan os.Signal, 1)
			signal.Notify(sigint, os.Interrupt)
			go func() {
				<-sigint
				if err := httpServer.Shutdown(context.Background()); err != nil {
					log.Fatalf("HTTP Server Shutdown Error: %v", err)
				}
				close(idleConnectionsClosed)
			}()

			if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
				log.Fatalf("HTTP server ListenAndServe Error: %v", err)
			}

			<-idleConnectionsClosed

			if conf.Connection.SQL.Enabled {
				if err := entgo.Close(); err != nil {
					log.Fatal(err)
				}
			}
		},
	}
	return cmd
}
