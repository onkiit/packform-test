package delivery

import "context"

type Delivery struct {
	ID                int `json:"id" csv:"id"`
	DeliveredQuantity int `json:"delivered_quantity" csv:"delivered_quantity"`
	OrderItemID       int `json:"order_item_id" csv:"order_item_id"`
}

type Store interface {
	BulkCreate(context.Context, []Delivery) error
}
