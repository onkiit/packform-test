package delivery

import (
	"context"
	"log"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

type deliveryEnt struct{}

func (d *deliveryEnt) BulkCreate(ctx context.Context, datas []Delivery) error {
	return entgo.WithTx(ctx, entgo.DB(), func(tx *ent.Tx) error {
		for _, data := range datas {
			_, err := tx.Delivery.
				Create().
				SetID(data.ID).
				SetDeliveredQuantity(data.DeliveredQuantity).
				SetOrderItemsID(data.OrderItemID).
				Save(ctx)
			if err != nil {
				log.Println("bulk delivery", err)
				return err
			}
		}

		return nil
	})
}

func NewStoreEnt() Store {
	return &deliveryEnt{}
}
