package helper

import (
	"net/http"
	"strconv"
	"time"
)

type Converter interface {
	ToString(...string) string
	ToInt(...int) int
	ToFloat64(...float64) float64
	ToTime(...time.Time) time.Time
}

type conv struct {
	value string
}

func (c *conv) ToString(fval ...string) string {
	if c.value == "" && len(fval) != 0 {
		return fval[0]
	}

	return c.value
}

func (c *conv) ToInt(fval ...int) int {
	v, err := strconv.Atoi(c.value)
	if err == nil {
		return v
	}

	if len(fval) != 0 {
		return fval[0]
	}

	return 0
}

func (c *conv) ToFloat64(fval ...float64) float64 {
	v, err := strconv.ParseFloat(c.value, 64)
	if err == nil {
		return v
	}

	if len(fval) != 0 {
		return fval[0]
	}

	return 0.0
}

func (c *conv) ToTime(fval ...time.Time) time.Time {
	t, err := time.Parse(time.RFC3339Nano, c.value)
	if err == nil {
		return t
	}

	if len(fval) > 0 {
		return fval[0]
	}

	return time.Time{}
}

func QueryParams(r *http.Request, key string) Converter {
	if r == nil {
		return &conv{value: ""}
	}

	return &conv{value: r.URL.Query().Get(key)}
}
