package helper

import (
	"os"

	_ "github.com/lib/pq"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

func OpenDB() error {
	if err := entgo.OpenConnection(entgo.Options{
		Driver: os.Getenv("DB_DRIVER"),
		URI:    os.Getenv("DB_URI"),
	}); err != nil {
		return err
	}

	return nil
}
