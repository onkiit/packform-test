package middleware

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func TraceMiddleware() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			log.Printf("[%s] Addr: %s Path: %s", req.Method, req.RemoteAddr, req.RequestURI)
			next.ServeHTTP(w, req)
		})
	}
}
