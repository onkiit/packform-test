package customer

import (
	"context"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

type customerEnt struct{}

func (c *customerEnt) BulkCreate(ctx context.Context, datas []Customer) error {
	return entgo.WithTx(ctx, entgo.DB(), func(tx *ent.Tx) error {
		for _, data := range datas {
			_, err := tx.Customer.
				Create().
				SetID(data.ID).
				SetLogin(data.Login).
				SetPassword(data.Password).
				SetName(data.Name).
				SetCreditCards(data.CreditCards).
				SetCompaniesID(data.CompanyID).
				Save(ctx)
			if err != nil {
				return err
			}
		}

		return nil
	})
}

func NewStoreEnt() Store {
	return &customerEnt{}
}
