package customer

import (
	"context"

	"gitlab.com/onkiit/packform-test/server/pkg/company"
)

type Customer struct {
	ID          int      `json:"id" csv:"id"`
	Login       string   `json:"login" csv:"login"`
	Password    string   `json:"password" csv:"password"`
	Name        string   `json:"name" csv:"name"`
	CreditCards []string `json:"credit_cards" csv:"credit_cards"`
	CompanyID   int      `json:"company_id" csv:"company_id"`
}

type CustomerResult struct {
	ID          int             `json:"id"`
	Login       string          `json:"login"`
	Password    string          `json:"password"`
	Name        string          `json:"name"`
	CreditCards []string        `json:"credit_cards"`
	Company     company.Company `json:"company"`
}

type Store interface {
	BulkCreate(context.Context, []Customer) error
}
