package customer

import (
	"context"
	"testing"

	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
	"gitlab.com/onkiit/packform-test/server/pkg/helper"
)

type args struct {
	ctx   context.Context
	datas []Customer
}

type testFunc func(test) func(*testing.T)

type test struct {
	name     string
	args     args
	wantErr  bool
	testFunc testFunc
}

func Test_BulkCreate(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	if err := helper.OpenDB(); err != nil {
		t.Error(err)
	}
	defer entgo.Close()
	ctx := context.Background()

	tests := []test{
		{
			name: "test bulk insert customer",
			args: args{
				ctx: ctx,
				datas: []Customer{
					{
						ID:          1,
						Login:       "dummy",
						Password:    "123123",
						Name:        "Dummy",
						CreditCards: []string{"****-****-5555"},
						CompanyID:   1,
					},
				},
			},
			testFunc: func(tt test) func(*testing.T) {
				return func(t2 *testing.T) {
					store := NewStoreEnt()

					if err := store.BulkCreate(tt.args.ctx, tt.args.datas); err != nil {
						t2.Error(err)
					}

					records, err := entgo.DB().Customer.Query().Count(tt.args.ctx)
					if err != nil {
						t2.Error(err)
					}

					if len(tt.args.datas) != records {
						t2.Errorf("expected records %d but got %d", len(tt.args.datas), records)
					}

					for _, data := range tt.args.datas {
						if err := entgo.DB().Customer.DeleteOneID(data.ID); err != nil {
							t2.Error(err)
						}
					}
				}
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, tt.testFunc(tt))
	}

}
