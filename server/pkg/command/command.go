package command

import (
	"github.com/spf13/cobra"
	"gitlab.com/onkiit/packform-test/server/config"
)

var _cmds []CommandFunc

type CommandFunc func(*config.Configuration) *cobra.Command

func Register(c CommandFunc) {
	_cmds = append(_cmds, c)
}

func Commands() []CommandFunc {
	return _cmds
}
