package command

import (
	"context"
	"log"
	"os"

	"github.com/gocarina/gocsv"
	"github.com/spf13/cobra"
	"gitlab.com/onkiit/packform-test/server/config"
	"gitlab.com/onkiit/packform-test/server/pkg/company"
	"gitlab.com/onkiit/packform-test/server/pkg/customer"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
	"gitlab.com/onkiit/packform-test/server/pkg/delivery"
	"gitlab.com/onkiit/packform-test/server/pkg/order"
	"gitlab.com/onkiit/packform-test/server/pkg/order/orderitem"
)

func parseCsv(path string, target any) error {
	f, err := os.OpenFile(path, os.O_RDWR, os.ModePerm)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := gocsv.UnmarshalFile(f, target); err != nil {
		return err
	}

	return nil
}

func migrateCompany(ctx context.Context) error {
	var companies []company.Company
	if err := parseCsv("../../assets/companies.csv", &companies); err != nil {
		return err
	}

	store := company.NewStoreEnt()
	if err := store.BulkCreate(ctx, companies); err != nil {
		return err
	}

	log.Println("companies migrated")

	return nil
}

func migrateCustomer(ctx context.Context) error {
	var customers []customer.Customer
	if err := parseCsv("../../assets/customers.csv", &customers); err != nil {
		return err
	}

	store := customer.NewStoreEnt()
	if err := store.BulkCreate(ctx, customers); err != nil {
		return err
	}

	log.Println("customers migrated")

	return nil
}

func migrateOrder(ctx context.Context) error {
	var orders []order.Order
	if err := parseCsv("../../assets/orders.csv", &orders); err != nil {
		return err
	}

	store := order.NewStoreEnt()
	if err := store.BulkCreate(ctx, orders); err != nil {
		return err
	}

	log.Println("orders migrated")

	return nil
}

func migrateOrderItem(ctx context.Context) error {
	var items []orderitem.OrderItem
	if err := parseCsv("../../assets/order_items.csv", &items); err != nil {
		return err
	}

	store := orderitem.NewStoreEnt()
	if err := store.BulkCreate(ctx, items); err != nil {
		return err
	}

	log.Println("order items migrated")

	return nil
}

func migrateDelivery(ctx context.Context) error {
	var deliveries []delivery.Delivery
	if err := parseCsv("../../assets/deliveries.csv", &deliveries); err != nil {
		return err
	}

	store := delivery.NewStoreEnt()
	if err := store.BulkCreate(ctx, deliveries); err != nil {
		return err
	}

	log.Println("deliveries migrated")

	return nil
}

func dataMigration(conf *config.Configuration) *cobra.Command {
	var dbUri, dbDriver string
	var useConfig bool
	c := &cobra.Command{
		Use:   "data-migrate",
		Short: "database schema migration",
		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()

			if err := migrateCompany(ctx); err != nil {
				log.Fatal("migrate company", err)
			}

			if err := migrateCustomer(ctx); err != nil {
				log.Fatal("migrate customer", err)
			}

			if err := migrateOrder(ctx); err != nil {
				log.Fatal("migrate order", err)
			}

			if err := migrateOrderItem(ctx); err != nil {
				log.Fatal("migrate order item", err)
			}

			if err := migrateDelivery(ctx); err != nil {
				log.Fatal("migrate delivery", err)
			}
		},
	}

	c.PersistentPreRun = func(cmd *cobra.Command, args []string) {
		opts := entgo.Options{
			Driver: dbDriver,
			URI:    dbUri,
		}
		if useConfig {
			opts.Driver = conf.Connection.SQL.Driver
			opts.URI = conf.Connection.SQL.URI
		}
		if err := entgo.OpenConnection(opts); err != nil {
			log.Println("open failed", err)
			return
		}
	}

	c.PersistentPostRun = func(cmd *cobra.Command, args []string) {
		if err := entgo.Close(); err != nil {
			log.Println("db close", err)
			return
		}
	}

	c.PersistentFlags().StringVarP(&dbDriver, "db-driver", "", "postgres", "Define database driver")
	c.PersistentFlags().StringVarP(&dbUri, "db-uri", "", "", "Define database connection URI")
	c.PersistentFlags().BoolVarP(&useConfig, "use-config-file", "", true, "By default use config file for db connnection properties")

	return c
}

func init() {
	Register(dataMigration)
}
