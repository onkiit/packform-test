package command

import (
	"context"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/onkiit/packform-test/server/config"
	"gitlab.com/onkiit/packform-test/server/ent/migrate"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

func dbSchemaMigrate(conf *config.Configuration) *cobra.Command {
	var dbUri, dbDriver string
	var useConfig bool
	c := &cobra.Command{
		Use:   "db-migrate",
		Short: "database schema/table migration",
		Run: func(cmd *cobra.Command, args []string) {
			err := entgo.DB().Schema.Create(
				context.Background(),
				migrate.WithDropIndex(true),
				migrate.WithDropColumn(true),
			)
			if err != nil {
				log.Fatalf("failed creating schema resources: %v", err)
				return
			}
			log.Println("schema migration finished")
		},
	}

	c.PersistentPreRun = func(cmd *cobra.Command, args []string) {
		opts := entgo.Options{
			Driver: dbDriver,
			URI:    dbUri,
		}
		if useConfig {
			opts.Driver = conf.Connection.SQL.Driver
			opts.URI = conf.Connection.SQL.URI
		}
		if err := entgo.OpenConnection(opts); err != nil {
			log.Println("open failed", err)
			return
		}
	}

	c.PersistentPostRun = func(cmd *cobra.Command, args []string) {
		if err := entgo.Close(); err != nil {
			log.Println("db close", err)
			return
		}
	}

	c.PersistentFlags().StringVarP(&dbDriver, "db-driver", "", "postgres", "Define database driver")
	c.PersistentFlags().StringVarP(&dbUri, "db-uri", "", "", "Define database connection URI")
	c.PersistentFlags().BoolVarP(&useConfig, "use-config-file", "", true, "By default use config file for db connnection properties")

	return c
}

func init() {
	Register(dbSchemaMigrate)
}
