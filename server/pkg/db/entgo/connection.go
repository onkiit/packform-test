package entgo

import (
	"log"

	"gitlab.com/onkiit/packform-test/server/ent"
)

var entClient *ent.Client

type Options struct {
	URI    string
	Driver string
}

func OpenConnection(opts Options) error {
	c, err := ent.Open(opts.Driver, opts.URI)
	if err != nil {
		return err
	}
	entClient = c
	log.Println("database connection established...")

	return nil
}

func DB() *ent.Client {
	return entClient
}

func Close() error {
	log.Println("connection closed ")
	return entClient.Close()
}
