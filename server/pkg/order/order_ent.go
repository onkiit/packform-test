package order

import (
	"context"
	"math"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/ent/company"
	"gitlab.com/onkiit/packform-test/server/ent/customer"
	"gitlab.com/onkiit/packform-test/server/ent/order"
	"gitlab.com/onkiit/packform-test/server/ent/orderitem"
	"gitlab.com/onkiit/packform-test/server/ent/predicate"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

type orderEnt struct{}

func (o orderEnt) BulkCreate(ctx context.Context, datas []Order) error {
	return entgo.WithTx(ctx, entgo.DB(), func(tx *ent.Tx) error {
		for _, data := range datas {
			_, err := tx.Order.
				Create().
				SetID(data.ID).
				SetOrderName(data.OrderName).
				SetCustomersID(data.CustomerID).
				SetCreatedAt(data.CreateAt).
				Save(ctx)
			if err != nil {
				return err
			}
		}

		return nil
	})
}

func (o orderEnt) Find(ctx context.Context, criteria SearchCriteria) (SearchResult, error) {
	filters := []predicate.Order{
		order.Or(
			// find by order name
			order.OrderNameContainsFold(criteria.Keyword),
			order.HasCustomersWith(
				customer.Or(
					// find by customer name
					customer.NameContainsFold(criteria.Keyword),
					customer.HasCompaniesWith(
						// find by company name
						company.NameContainsFold(criteria.Keyword),
					),
				),
			),
			order.HasOrderItemsWith(orderitem.ProductContainsFold(criteria.Keyword)),
		),
	}

	if !criteria.StartDate.IsZero() && !criteria.EndDate.IsZero() {
		filters = append(filters, order.And(
			order.CreatedAtGTE(criteria.StartDate),
			order.CreatedAtLTE(criteria.EndDate),
		))
	}

	orderQuery := entgo.DB().Order.
		Query().
		Where(
			filters...,
		).
		WithCustomers(
			func(cq *ent.CustomerQuery) {
				cq.WithCompanies()
			},
		).
		WithOrderItems(
			func(oiq *ent.OrderItemQuery) {
				oiq.WithDeliveries()
			},
		)

	totalRecords, err := orderQuery.Count(ctx)
	if err != nil {
		return SearchResult{}, err
	}

	if criteria.Limit != -1 {
		orderQuery.
			Limit(criteria.Limit)
	}

	orders, err := orderQuery.
		Offset(criteria.Skip).
		Order(ent.Desc(order.FieldCreatedAt)).
		All(ctx)
	if err != nil {
		return SearchResult{}, err
	}

	return SearchResult{
		Records:      FromEnt(orders),
		TotalPage:    int(math.Ceil(float64(len(orders)) / float64(criteria.Limit))),
		TotalRecords: totalRecords,
	}, nil
}

func NewStoreEnt() Store {
	return &orderEnt{}
}
