package order

import (
	"context"
	"time"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/pkg/company"
	"gitlab.com/onkiit/packform-test/server/pkg/customer"
	"gitlab.com/onkiit/packform-test/server/pkg/delivery"
	"gitlab.com/onkiit/packform-test/server/pkg/order/orderitem"
)

type Datetime struct {
	time.Time
}

const format = "2006-01-02T15:04:05Z"

func (d *Datetime) UnmarshalCSV(data []byte) error {
	tt, err := time.Parse(format, string(data))
	if err != nil {
		return err
	}
	*d = Datetime{Time: tt}

	return nil
}

type Order struct {
	ID         int       `json:"id" csv:"id"`
	CreateAt   time.Time `json:"created_at" csv:"created_at"`
	OrderName  string    `json:"order_name" csv:"order_name"`
	CustomerID int       `json:"customer_id" csv:"customer_id"`
}

type OrderResult struct {
	ID         int                         `json:"id"`
	CreateAt   time.Time                   `json:"create_at"`
	OrderName  string                      `json:"order_name"`
	Customer   customer.CustomerResult     `json:"customer"`
	OrderItems []orderitem.OrderItemResult `json:"order_items"`
}

type SearchCriteria struct {
	Keyword   string
	StartDate time.Time
	EndDate   time.Time
	Skip      int
	Limit     int
}

type SearchResult struct {
	Records      []OrderResult `json:"records"`
	TotalPage    int           `json:"total_page"`
	TotalRecords int           `json:"total_records"`
}

type Store interface {
	BulkCreate(context.Context, []Order) error
	Find(context.Context, SearchCriteria) (SearchResult, error)
}

func FromEnt(results []*ent.Order) []OrderResult {
	var orders []OrderResult
	for _, v := range results {
		odr := OrderResult{
			ID:        v.ID,
			CreateAt:  v.CreatedAt,
			OrderName: v.OrderName,
			Customer: customer.CustomerResult{
				ID:    v.Edges.Customers.ID,
				Login: v.Edges.Customers.Login,
				Name:  v.Edges.Customers.Name,
				Company: company.Company{
					ID:   v.Edges.Customers.Edges.Companies.ID,
					Name: v.Edges.Customers.Edges.Companies.Name,
				},
			},
		}

		var orderItems []orderitem.OrderItemResult
		if len(v.Edges.OrderItems) > 0 {
			for _, item := range v.Edges.OrderItems {
				ordItem := orderitem.OrderItemResult{
					ID:           item.ID,
					PricePerUnit: item.PricePerUnit,
					Quantity:     item.Quantity,
					Product:      item.Product,
				}

				if len(item.Edges.Deliveries) > 0 {
					var deliveries []delivery.Delivery
					for _, del := range item.Edges.Deliveries {
						deliveries = append(deliveries, delivery.Delivery{
							ID:                del.ID,
							DeliveredQuantity: del.DeliveredQuantity,
						})
					}

					ordItem.Deliveries = append(ordItem.Deliveries, deliveries...)
				}

				orderItems = append(orderItems, ordItem)
			}
		}
		odr.OrderItems = append(odr.OrderItems, orderItems...)

		orders = append(orders, odr)
	}

	return orders
}
