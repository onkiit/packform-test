package orderitem

import (
	"context"
	"strconv"

	"gitlab.com/onkiit/packform-test/server/pkg/delivery"
)

type NilFloat struct {
	Val float64
}

func (d *NilFloat) UnmarshalCSV(data []byte) error {
	str := string(data)
	if str == "" {
		str = "0"
	}

	v, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return err
	}
	d.Val = v

	return nil
}

type OrderItem struct {
	ID           int     `json:"id" csv:"id"`
	OrderID      int     `json:"order_id" csv:"order_id"`
	PricePerUnit float64 `json:"price_per_unit" csv:"price_per_unit"`
	Quantity     int     `json:"quantity" csv:"quantity"`
	Product      string  `json:"product" csv:"product"`
}

type OrderItemResult struct {
	ID           int                 `json:"id"`
	OrderID      int                 `json:"order_id"`
	PricePerUnit float64             `json:"price_per_unit"`
	Quantity     int                 `json:"quantity"`
	Product      string              `json:"product"`
	Deliveries   []delivery.Delivery `json:"deliveries"`
}

type Store interface {
	BulkCreate(context.Context, []OrderItem) error
}
