package orderitem

import (
	"context"

	"gitlab.com/onkiit/packform-test/server/ent"
	"gitlab.com/onkiit/packform-test/server/pkg/db/entgo"
)

type orderitemEnt struct{}

func (o *orderitemEnt) BulkCreate(ctx context.Context, datas []OrderItem) error {
	return entgo.WithTx(ctx, entgo.DB(), func(tx *ent.Tx) error {
		for _, data := range datas {
			_, err := tx.OrderItem.
				Create().
				SetID(data.ID).
				SetOrdersID(data.OrderID).
				SetPricePerUnit(data.PricePerUnit).
				SetProduct(data.Product).
				SetQuantity(data.Quantity).
				Save(ctx)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func NewStoreEnt() Store {
	return &orderitemEnt{}
}
