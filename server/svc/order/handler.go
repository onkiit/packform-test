package order

import (
	"encoding/json"
	"net/http"
	"path"

	"gitlab.com/onkiit/packform-test/server/config"
	"gitlab.com/onkiit/packform-test/server/pkg/app/api"
	"gitlab.com/onkiit/packform-test/server/pkg/helper"
	"gitlab.com/onkiit/packform-test/server/pkg/order"
)

type OrderService struct {
	orderStore order.Store
}

func (o *OrderService) GetOrder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		criteria := order.SearchCriteria{
			Keyword:   helper.QueryParams(r, "keyword").ToString(),
			Skip:      helper.QueryParams(r, "skip").ToInt(),
			Limit:     helper.QueryParams(r, "limit").ToInt(),
			StartDate: helper.QueryParams(r, "start_date").ToTime(),
			EndDate:   helper.QueryParams(r, "end_date").ToTime(),
		}

		res, err := o.orderStore.Find(r.Context(), criteria)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		responseByte, err := json.Marshal(res)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Write(responseByte)
	})
}

func (o *OrderService) AppendRouter(base string) []api.APIRouter {
	return []api.APIRouter{
		{
			Path:    path.Join(base, "/orders"),
			Methods: http.MethodGet,
			Handler: o.GetOrder(),
		},
	}
}

func New(conf *config.Configuration) api.Router {
	return &OrderService{
		orderStore: order.NewStoreEnt(),
	}
}

func init() {
	api.Register(New)
}
