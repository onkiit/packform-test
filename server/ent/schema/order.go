package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Order holds the schema definition for the Order entity.
type Order struct {
	ent.Schema
}

// Fields of the Order.
func (Order) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Immutable().Unique(),
		field.String("order_name"),
		field.Time("created_at"),
	}
}

// Edges of the Order.
func (Order) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("customers", Customer.Type).Ref("orders").Unique(),
		edge.To("order_items", OrderItem.Type),
	}
}

// OrderItem holds the schema definition for the Order entity.
type OrderItem struct {
	ent.Schema
}

// Fields of the Order.
func (OrderItem) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Immutable().Unique(),
		field.String("product"),
		field.Float("price_per_unit"),
		field.Int("quantity"),
	}
}

// Edges of the Order.
func (OrderItem) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("orders", Order.Type).Ref("order_items").Unique(),
		edge.To("deliveries", Delivery.Type),
	}
}
