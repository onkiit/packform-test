package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Customer holds the schema definition for the Customer entity.
type Customer struct {
	ent.Schema
}

// Fields of the Customer.
func (Customer) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Immutable().Unique(),
		field.String("login").Unique(),
		field.String("password"),
		field.String("name"),
		field.JSON("credit_cards", []string{}),
	}
}

// Edges of the Customer.
func (Customer) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("companies", Company.Type).Unique().Ref("customers").Required(),
		edge.To("orders", Order.Type),
	}
}
